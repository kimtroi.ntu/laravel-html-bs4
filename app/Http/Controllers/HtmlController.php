<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;

class htmlController extends Controller
{
    public function index() {
        $page = Input::get('page');

        switch ($page) {
            case 'landing':
                return view('html.pages.landing');
                break;

            default :
                return view('html.pages.home');
                break;
        }
    }
}
