@include('html.blocks.common._header-landing')

<main id="main-content" class="main">
    @yield('content')
</main>

@include('html.blocks.common._footer-landing')