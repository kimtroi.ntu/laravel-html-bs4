@include('html.blocks.common._header')

<main id="main-content" class="main">
    @yield('content')
</main>

@include('html.blocks.common._footer')