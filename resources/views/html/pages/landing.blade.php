{{--extends layout--}}
@extends('html._layout-landing')

{{--set variables--}}
@section('metaTitle', 'Landing')
@section('bodyClass', 'landing')

{{--include blocks--}}
@section('content')
    <section class="container">
        <h2>This is content landing!</h2>
    </section>
@endsection