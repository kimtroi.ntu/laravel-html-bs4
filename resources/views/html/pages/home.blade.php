{{--extends layout--}}
@extends('html._layout')

{{--set variables--}}
@section('metaTitle', 'Home')
@section('bodyClass', 'home')

{{--include blocks--}}
@section('content')
    <section class="container">
        <h2>This is content!</h2>
    </section>
@endsection