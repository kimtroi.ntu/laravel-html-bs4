<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('metaTitle') - HTML Template Landing</title>

    <!-- Fonts -->
    <!-- <link href="https://fonts.googleapis.com/css?family=Lato:400,500,600,700,800,900" rel="stylesheet"> -->

    <!-- Styles -->
    <link rel="stylesheet" href="{{ mix('assets/css/landing.css') }}">
</head>
<body class="@yield('bodyClass')">

<header>
    <div class="container">
        <h1>This is header landing!</h1>
    </div>
</header>