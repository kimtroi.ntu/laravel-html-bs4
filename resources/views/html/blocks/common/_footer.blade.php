<footer>
    <div class="container">
        <p>This is footer!</p>
    </div>
</footer>

<!-- Js -->
<script src="{{ mix('assets/js/app.js') }}"></script>

<script>
    jQuery(window).on('load', function () {
        appFunctions.exportFunction();
    });
</script>

</body>
</html>